package com.bmc.project.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import static com.bmc.project.constants.ApiOperationDesc.API_DESC_CUSTOMER_CONTROLLER;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bmc.project.constants.ApiOperationDesc;
import com.bmc.project.models.Customer;
import com.bmc.project.services.CustomerService;

@Api(API_DESC_CUSTOMER_CONTROLLER)
@RestController
@RequestMapping("customers")
public class CustomerController implements ApiOperationDesc {

	@Autowired
	CustomerService customerService;

	// Get in Json format
	@ApiOperation(value = API_DESC_GET_CUSTOMER_IN_JSON)
	@GetMapping(value = "/{firstName}", produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<Customer> getCustomerInJSON(
			@PathVariable final String firstName) {
		final Customer customer = customerService
				.findCustomerByFirstName(firstName);
		return new ResponseEntity<Customer>(customer, HttpStatus.OK);
	}

	// Get in XML format
	@ApiOperation(value = API_DESC_GET_CUSTOMER_IN_XML)
	@RequestMapping(value = "/{firstName}.xml", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
	public @ResponseBody ResponseEntity<Customer> getCustomersInXML(
			@PathVariable final String firstName) {
		final Customer customer = customerService
				.findCustomerByFirstName(firstName);
		return new ResponseEntity<Customer>(customer, HttpStatus.OK);
	}

	// Post
	@ApiOperation(value = API_DESC_SAVE_CUSTOMER_IN_JSON)
	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<Customer> saveCustomerInJson(
			@RequestBody final Customer customer) {
		final Customer customerSave = customerService.saveCustomer(customer);
		return new ResponseEntity<Customer>(customerSave, HttpStatus.OK);
	}

	// Put
	@ApiOperation(value = API_DESC_UPDATE_CUSTOMER_IN_JSON)
	@RequestMapping(value = "/update/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<Customer> updateCustomerInJson(
			@RequestBody final Customer customer, @PathVariable final long id) {

		final Customer customerById = customerService.findCustomerById(id);
		if (customerById != null) {
			customer.setId(id);
			final Customer customerSave = customerService
					.updateCustomer(customer);
			return new ResponseEntity<Customer>(customerSave, HttpStatus.OK);
		}

		return new ResponseEntity<Customer>(customer, HttpStatus.NOT_FOUND);
	}

	//Delete
	@ApiOperation(value = API_DESC_DELETE_CUSTOMER_IN_JSON)
	@RequestMapping(value = "/delete/{firstName}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<Customer> updateCustomerInJson(
			@PathVariable final String firstName) {

		final Customer customerByFirstName = customerService
				.findCustomerByFirstName(firstName);
		if (customerByFirstName != null) {
			customerService.deleteCustomerByFirstName(customerByFirstName);
			return new ResponseEntity<Customer>(customerByFirstName, HttpStatus.OK);
		}
		return new ResponseEntity<Customer>(HttpStatus.NOT_FOUND);
	}

}
