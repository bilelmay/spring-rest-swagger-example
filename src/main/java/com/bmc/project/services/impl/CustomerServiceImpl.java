package com.bmc.project.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bmc.project.models.Customer;
import com.bmc.project.repositories.CustomerRepository;
import com.bmc.project.services.CustomerService;

@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	CustomerRepository customerRepository;

	public Customer findCustomerByFirstName(String firstName) {
		return customerRepository.findByFirstName(firstName);
	}

	public Customer saveCustomer(Customer customer) {
		return customerRepository.saveAndFlush(customer);
	}

	public Customer findCustomerById(Long id) {
		return customerRepository.findById(id).get();
	}

	public Customer updateCustomer(Customer customer) {
		return customerRepository.save(customer);
	}

	public void deleteCustomerByFirstName(Customer customer) {
		customerRepository.delete(customer);
	}

	
}