package com.bmc.project.services;

import com.bmc.project.models.Customer;

public interface CustomerService {

	Customer findCustomerByFirstName(String firstName);

	Customer saveCustomer(Customer customer);

	Customer findCustomerById(Long id);

	Customer updateCustomer(Customer customer);

	void deleteCustomerByFirstName(Customer customerByFirstName);

}