package com.bmc.project.configs;

import java.util.Collections;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.bmc.project.constants.SpringFoxConfigParams;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SpringFoxConfig implements SpringFoxConfigParams {
    
    @Bean
    public Docket apiDocket() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                 .apis(RequestHandlerSelectors.basePackage(CUSTOMER_API_DOCKET_BASE_PACKAGE))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(getApiInfo());
    }

    private ApiInfo getApiInfo() {
        
    	return new ApiInfo(
        		CUSTOMER_APIINFO_TITLE,
        		CUSTOMER_APIINFO_DESCIPRION,
        		CUSTOMER_APIINFO_VERSION,
        		CUSTOMER_APIINFO_TERMS,
                new Contact(CUSTOMER_APIINFO_CONTACT_NAME, CUSTOMER_APIINFO_CONTACT_URL, CUSTOMER_APIINFO_CONTACT_EMAIL),
                CUSTOMER_APIINFO_LICENSE,
                CUSTOMER_APIINFO_LICENSE_URL,
                Collections.EMPTY_LIST
        );
    }
}