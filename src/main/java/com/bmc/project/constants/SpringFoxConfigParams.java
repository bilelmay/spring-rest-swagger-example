package com.bmc.project.constants;

public interface SpringFoxConfigParams {
	public static final String CUSTOMER_APIINFO_TITLE = "Api Customer Title";
	public static final String CUSTOMER_APIINFO_DESCIPRION = "A Customer WebServices";
	public static final String CUSTOMER_APIINFO_VERSION = "1.0";
	public static final String CUSTOMER_APIINFO_TERMS = "www.teamwill-consulting.fr";
	public static final String CUSTOMER_APIINFO_CONTACT_NAME = "Admin";
	public static final String CUSTOMER_APIINFO_CONTACT_URL = "http://www.teamwill-consulting.fr/fr/";
	public static final String CUSTOMER_APIINFO_CONTACT_EMAIL = "bilel.may@teamwill.consulting";
	public static final String CUSTOMER_APIINFO_LICENSE = "";
	public static final String CUSTOMER_APIINFO_LICENSE_URL = "";
	public static final String CUSTOMER_API_DOCKET_BASE_PACKAGE = "com.teamwill.project.controller";

}