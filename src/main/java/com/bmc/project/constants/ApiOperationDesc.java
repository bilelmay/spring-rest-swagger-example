package com.bmc.project.constants;


public interface ApiOperationDesc {
	
	public static final String API_DESC_CUSTOMER_CONTROLLER = "API for Customer Crud Operation operations.";
	public static final String API_DESC_GET_CUSTOMER_IN_JSON = "Recover a Customer with its Name and return Json";
	public static final String API_DESC_GET_CUSTOMER_IN_XML = "Recover a Customer with its Name and return XML";
	public static final String API_DESC_SAVE_CUSTOMER_IN_JSON = "Save a Customer with its params(id, firstName, lastname, email) and return XML";
	public static final String API_DESC_UPDATE_CUSTOMER_IN_JSON = "Update a Customer with its params(id, firstName, lastname, email) and return XML";
	public static final String API_DESC_DELETE_CUSTOMER_IN_JSON = "Delete a Customer with its firstName and return XML";
	
	public static final String API_MODEL_CUSTOMER_DESC = "Model for showing Customer details";
	
	
}