package com.bmc.project.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import com.bmc.project.models.Customer;


@Component
public interface CustomerRepository extends JpaRepository<Customer, Long> {

	long deleteByLastName(String lastName);

	Customer findByFirstName(String firstName);

	Customer findByEmail(String email);

	List<Customer> removeByLastName(String lastName);

	Customer saveAndFlush(Customer customer);
}