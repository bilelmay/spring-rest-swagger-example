package com.bmc.project.models;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ApiModel(description = "Model for showing user details")
@XmlRootElement(name = "Customer")
@Entity(name = "customer")
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Customer implements Serializable {
	private static final long serialVersionUID = 8645221892058336869L;

	@ApiModelProperty(position = 1, required = true, value = "Customer Id")
	@Id
	@GeneratedValue
	@Column(name = "id")
	private Long id;

	@ApiModelProperty(position = 2, required = true, value = "Customer First Name")
	@Column(name = "firstName")
	private String firstName;

	@ApiModelProperty(position = 3, required = true, value = "Customer Last Name")
	@Column(name = "lastName")
	private String lastName;

	@ApiModelProperty(position = 4, required = true, value = "Customer Email")
	@Column(name = "email")
	private String email;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	
	

}